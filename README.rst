Electus
========

Electus is a package for generating alerts based on combinations of indicators. It allows the user to create a library of flexible behavioural signatures 
that can be reused in different contexts. To install it just run

.. code-block::

    pip install electus

To get started the documentation is available on `Read the Docs <http://electus.readthedocs.io/en/latest/>`_.