import pytest

from electus import LibraryDefinition

indicators = [{'field': 'process', 'value': 'powershell.exe'},
              {'field': 'parent_process', 'value': 'winword.exe'}]
definition = {'datetime_field': 'datetime', 'weight': 10.0,
              'indicators': indicators}

lib_def = LibraryDefinition(name='word_launching_powershell', definition=definition)


def test_name():
    assert lib_def.name == 'word_launching_powershell'


def test_datetime_field():
    assert lib_def.dt_field == 'datetime'


def test_weight():
    assert lib_def.weight == 10


def test_default_weight():
    """The weight key should default to 5 when not specified"""
    definition = {'datetime_field': 'datetime', 'indicators': indicators}

    lib_def = LibraryDefinition(name='word_launching_powershell', definition=definition)

    assert lib_def.weight == 5


def test_name_as_number():
    with pytest.raises(ValueError):
        LibraryDefinition(name=12345, definition=definition)

def test_invalid_name():
    with pytest.raises(ValueError):
        LibraryDefinition(name="this contains spaces", definition=definition)

def test_dt_field_exists():
    with pytest.raises(ValueError):
        d = {'weight': 10.0, 'indicators': indicators}
        LibraryDefinition(name='foo', definition=d)


def test_dt_field_type():
    with pytest.raises(ValueError):
        d = {'weight': 10.0, 'indicators': indicators, 'datetime_field': 12345}
        LibraryDefinition(name='dt_test', definition=d)


def test_weight_type():
    with pytest.raises(ValueError):
        d = {'indicators': indicators, 'datetime_field': 'datetime', 'weight': '20'}
        LibraryDefinition(name='weight_test_type', definition=d)


def test_all_indicators_match():
    """In this case both indicators match the event"""
    event = {'datetime': '2017-10-16T13:49:45.733992', 'parent_process': 'winword.exe', 'process': 'powershell.exe'}
    assert lib_def.eval_indicators(event) is True


def test_indicators_match_fails():
    """In this case the event matches on only one indicator"""
    event = {'datetime': '2017-10-16T13:49:45.733992', 'parent_process': 'explorer.exe', 'process': 'powershell.exe'}
    assert lib_def.eval_indicators(event) is False

def test_regex_eval():
    indicator_regex = {'field': 'process', 'value': '[A-Za-z\.]+', 'match': True, 'is_regex': True}

    definition = {'datetime_field': 'datetime', 'weight': 10.0, 'indicators': [indicator_regex]}
    lib_def = LibraryDefinition(name='regex_test', definition=definition)

    event = {'process': 'foo.exe'}

    assert lib_def.eval_indicators(event)

def test_regex_eval_failure():
    indicator_regex = {'field': 'process', 'value': '[0-9]+', 'match': True, 'is_regex': True}

    definition = {'datetime_field': 'datetime', 'weight': 10.0, 'indicators': [indicator_regex]}
    lib_def = LibraryDefinition(name='regex_test', definition=definition)

    event = {'process': 'foo.exe'}

    assert lib_def.eval_indicators(event) is False


def test_ignore_match():
    indicator_regex = {'field': 'process', 'value': 'foo.exe', 'match': False}

    definition = {'datetime_field': 'datetime', 'weight': 10.0, 'indicators': [indicator_regex]}
    lib_def = LibraryDefinition(name='regex_test', definition=definition)

    event = {'process': 'foo.exe'}

    assert lib_def.eval_indicators(event) is False


def test_case_sensitive_normal():
    indicator_case = {'field': 'process', 'value': 'PoWeRsHeLl.ExE', 'case_sensitive': False}

    definition = {'datetime_field': 'datetime', 'weight': 10.0, 'indicators': [indicator_case]}
    lib_def = LibraryDefinition(name='case_sensitive_test', definition=definition)

    event = {'datetime': '2017-10-16T13:49:45.733992', 'parent_process': 'winword.exe', 'process': 'POWERSHELL.EXE'}

    assert lib_def.eval_indicators(event) is True



