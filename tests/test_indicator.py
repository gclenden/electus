import pytest

from electus import Indicator

indicator_def = {'field': 'parent_process', 'value': 'SVCHOST.exe', 'match': False, 'is_regex': False, 'case_sensitive': False}
ind = Indicator(indicator_def)


def test_field():
    assert ind.field == 'parent_process'


def test_value():
    assert ind.value == 'svchost.exe'


def test_match():
    assert ind.match is False


def test_regex():
    assert ind.regex is False


def test_case_sensitive():
    assert ind.case_sensitive is False


def test_field_type():
    """Field must be a string"""
    with pytest.raises(ValueError):
        ind.field = 1


def test_match_type():
    """Match must be a boolean"""
    with pytest.raises(ValueError):
        ind.match = 'true'


def test_regex_type():
    """Regex must be a boolean"""
    with pytest.raises(ValueError):
        ind.regex = 'true'

def test_case_sensitive_type():
    """Case sensitive must be a boolean"""
    with pytest.raises(ValueError):
        ind.regex = 'true'