import pytest

from electus.combinations import Join, WeightedFilter, Sequence
from electus import LibraryDefinition
from electus.config import Library, Job
from electus.alert import Alert

indicators = [{'field': 'process', 'value': 'powershell.exe'},
              {'field': 'parent_process', 'value': 'winword.exe'}]
definition = {'datetime_field': 'datetime', 'weight': 10.0,
              'indicators': indicators}

lib = Library({'word_launching_powershell': definition})

def test_invalid_join_expr():
    # This is a valid job
    join_conf = {"winword_launching_powershell": {"type": "join", "features": ["word_launching_powershell"], "join_expression": "word_launching_powershell"}}
    test_job = Job("winword_launching_powershell", join_conf["winword_launching_powershell"], lib.library)
    # This should fail because the join expression has unbalanced parantheses
    with pytest.raises(ValueError):
        Join(name="test", definitions=test_job.features, join_expression="(word_launching_powershell")

def test_invalid_join_name():
    # This is a valid job
    join_conf = {"winword_launching_powershell": {"type": "join", "features": ["word_launching_powershell"], "join_expression": "word_launching_powershell"}}
    test_job = Job("winword_launching_powershell", join_conf["winword_launching_powershell"], lib.library)
    # This should fail because the name needs to be a non empty string
    with pytest.raises(ValueError):
        Join(name=1234, definitions=test_job.features, join_expression="word_launching_powershell")

def test_invalid_definitions():
    # This is a valid job
    join_conf = {"winword_launching_powershell": {"type": "join", "features": ["word_launching_powershell"], "join_expression": "word_launching_powershell"}}
    # This should fail because the definitions is not a list of LibraryDefinitions
    with pytest.raises(ValueError):
        Join(name="test", definitions=[1,2,3,4], join_expression="(word_launching_powershell")

def test_subsets():
    # This is a valid job
    join_conf = {"winword_launching_powershell": {"type": "join", "features": ["word_launching_powershell"], "join_expression": "word_launching_powershell"}}
    test_job = Job("winword_launching_powershell", join_conf["winword_launching_powershell"], lib.library)
    test_join = test_job.combination_engine

    test_join._subsets = [["A", "B"]]
    assert test_join.check_subset(["A", "B", "C"]) == True
    assert test_join.check_subset(["A"]) == False

def test_alert():
    # This is a valid job
    join_conf = {"winword_launching_powershell": {"type": "join", "features": ["word_launching_powershell"], "join_expression": "word_launching_powershell"}}
    test_job = Job("winword_launching_powershell", join_conf["winword_launching_powershell"], lib.library)
    test_join = test_job.combination_engine

    events = [{"parent_process": "winword.exe", "process": "powershell.exe"}]

    assert isinstance(test_join.alert(events), Alert)

def test_no_hit():
    # This is a valid job
    join_conf = {"winword_launching_powershell": {"type": "join", "features": ["word_launching_powershell"], "join_expression": "word_launching_powershell"}}
    test_job = Job("winword_launching_powershell", join_conf["winword_launching_powershell"], lib.library)
    test_join = test_job.combination_engine
    
    # Will not trigger an alert
    events = [{"parent_process": "explorer.exe", "process": "calc.exe"}]

    assert test_join.alert(events) is None

def test_no_events():
    join_conf = {"winword_launching_powershell": {"type": "join", "features": ["word_launching_powershell"], "join_expression": "word_launching_powershell"}}
    test_job = Job("winword_launching_powershell", join_conf["winword_launching_powershell"], lib.library)
    test_join = test_job.combination_engine
    
    # Will not trigger an alert
    events = None

    assert test_join.alert(events) is None    

