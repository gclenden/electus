import pytest
from electus import LibraryDefinition
from electus.combinations import WeightedFilter

indicators = [{'field': 'process', 'value': 'powershell.exe'},
              {'field': 'parent_process', 'value': 'winword.exe'}]
definition = {'datetime_field': 'datetime', 'weight': 10.0, 'indicators': indicators}

lib_def = LibraryDefinition('word_launching_powershell', definition)

events = [{'datetime': '2017-10-16T17:15:47.030114', 'parent_process': 'explorer.exe', 'process': 'winword.exe'},
          {'datetime': '2017-10-16T17:15:48.030123', 'parent_process': 'winword.exe', 'process': 'powershell.exe'}]

wf = WeightedFilter(name='testing', definitions=[lib_def], threshold=5)


def test_threshold():
    assert wf.threshold == 5.0


def test_threshold_type():
    with pytest.raises(ValueError):
        WeightedFilter(name='testing', definitions=[lib_def], threshold='2')


def test_definitions():
    assert isinstance(wf.definitions[0], LibraryDefinition)


def test_definitions_type():
    with pytest.raises(ValueError):
        WeightedFilter(name='testing', definitions=['not_a_library_definition'])


def test_definitions_container_type():
    with pytest.raises(ValueError):
        WeightedFilter(name='testing', definitions='not_a_list_or_library_definition')


def test_alert_on_latest():
    hits = wf.alert(events[:2], fetch_all_events=False)
    assert len(hits) == 1


def test_no_alert():
    hits = wf.alert(events[:1], fetch_all_events=False)
    assert hits is None


def test_fetch_all_alerts():
    e = events[-1]
    more_events = [e] + events
    hits = wf.alert(more_events, fetch_all_events=True)
    assert len(hits) == 2

