import pytest
from electus.combinations import Join
from boolean import ParseError

# Need to ensure order for testing
def to_set(nested):
    return set([tuple(sorted(x)) for x in nested])


def test_complex_vars():
    assert to_set(Join.parse_join_logic("(evil.exe and apples_are_good.bat and (oranges_are_better.ps1 or dodge.pdf.exe & legit.pdf.docx) or legit.pdf.docx and ip_127.0.0.1)")) == {("ip_127.0.0.1", "legit.pdf.docx"), ("apples_are_good.bat", "dodge.pdf.exe", "evil.exe", "legit.pdf.docx"), ("apples_are_good.bat", "evil.exe", "oranges_are_better.ps1")}
    assert to_set(Join.parse_join_logic("(evil.exe and (apples_are_good.bat or oranges_are_better.ps1))")) == {("apples_are_good.bat", "evil.exe"), ("evil.exe", "oranges_are_better.ps1")}


def test_alpha_vars():
    # Testing alpha vars
    assert to_set(Join.parse_join_logic("( A and ( B or C ) )")) == {("A", "B"), ("A", "C")}
    assert to_set(Join.parse_join_logic("( A and B and ( C or D and F ) or F and G )")) == {("A", "B", "C"), ("A", "B", "D", "F"), ("F", "G")}


def test_alphanumeric_vars():
    assert to_set(Join.parse_join_logic("( A1 and ( B2 or C3 ) )")) == {("A1", "B2"), ("A1", "C3")}
    assert to_set(Join.parse_join_logic("( A1 and B2 and ( C3 or D4 and F5 ) or F5 and G6 )")) == to_set([["A1", "B2", "C3"], ["A1", "B2", "D4", "F5"], ["F5", "G6"]])


def test_special_vars():
    assert to_set(Join.parse_join_logic("( A_1 and ( B_2 or C_3 ) )")) == {("A_1", "B_2"), ("A_1", "C_3")}
    assert to_set(Join.parse_join_logic("( A_1 and B_2 and ( C_3 or D_4 and F_5 ) or F_5 and G_6 )")) == {("A_1", "B_2", "C_3"), ("A_1", "B_2", "D_4", "F_5"), ("F_5", "G_6")}


def test_compact_vars():
    assert to_set(Join.parse_join_logic("(A_1 and (B_2 or C_3))")) == {("A_1", "B_2"), ("A_1", "C_3")}
    assert to_set(Join.parse_join_logic("(A_1 and B_2 and (C_3 or D_4 and F_5) or F_5 and G_6)")) == {("A_1", "B_2", "C_3"), ("A_1", "B_2", "D_4", "F_5"), ("F_5", "G_6")}


def test_special_character_logic():
    assert to_set(Join.parse_join_logic("(A_1 & (B_2 | C_3))")) == {("A_1", "B_2"), ("A_1", "C_3")}
    assert to_set(Join.parse_join_logic("(A_1 & B_2 and (C_3 | D_4 & F_5) | F_5 & G_6)")) == {("A_1", "B_2", "C_3"), ("A_1", "B_2", "D_4", "F_5"), ("F_5", "G_6")}


def test_parentheses_inbalance():
    with pytest.raises(ParseError):
        assert Join.parse_join_logic("(A_1 && B_2 and (C_3 || D_4 && F_5) || F_5 && G_6))") == [["A_1", "B_2", "C_3"], ["A_1", "B_2", "D_4", "F_5"], ["F_5", "G_6"]]
