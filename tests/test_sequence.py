from datetime import datetime, timedelta

import pytest
from electus import LibraryDefinition
from electus.combinations import Sequence

indicators_a = [{'field': 'process', 'value': 'a.exe'}]
definition_a = {'datetime_field': 'datetime', 'weight': 5.0, 'indicators': indicators_a}
lib_a = LibraryDefinition('suspicious_a', definition_a)

indicators_b = [{'field': 'process', 'value': 'b.exe'}]
definition_b = {'datetime_field': 'datetime', 'weight': 5.0, 'indicators': indicators_b}
lib_b = LibraryDefinition('suspicious_b', definition_b)


time_a = datetime.utcnow()
time_b = (time_a + timedelta(seconds=5)).isoformat()

time_a = time_a.isoformat()


events = [{'datetime': time_a, 'process': 'a.exe'},
          {'datetime': time_b, 'process': 'b.exe'}]

seq = Sequence(name='testing', definitions=[lib_a, lib_b], event_names=['suspicious_a', 'suspicious_b'], time_window=10)


def test_time_window():
    assert seq.time_window == 10


def test_event_names():
    assert seq.event_names == ['suspicious_a', 'suspicious_b']


def test_definitions():
    assert isinstance(seq.definitions[0], LibraryDefinition)


def test_definitions_type():
    with pytest.raises(ValueError):
        Sequence(name='testing', definitions=['not_a_library_definition'], event_names=['foo'], time_window=10)


def test_valid_event_names():
    with pytest.raises(ValueError):
        Sequence(name='testing', definitions=[lib_a], event_names=False, time_window=10)


def test_valid_time_window():
    with pytest.raises(ValueError):
        Sequence(name='testing', definitions=[lib_a], event_names=['foo'], time_window='not_a_valid_time_window')


def test_subsequence_matching():
    event_info_list = [(lib_a, datetime.utcnow()), (lib_b, datetime.utcnow() + timedelta(seconds=3))]

    assert seq.check_subsequence_match(event_info_list)


def test_alert():
    hits = seq.alert(events)
    assert hits is not None


def test_reversed_sequence():
    """Should return no results because the sequence of events is reversed"""
    reversed_seq = Sequence(name='testing', definitions=[lib_a, lib_b], event_names=['suspicious_b', 'suspicious_b'], time_window=10)

    hits = reversed_seq.alert(events)

    assert hits is None


def test_short_sequence():
    """Should fail because the first event is outside the time window"""
    short_seq = Sequence(name='testing', definitions=[lib_a, lib_b], event_names=['suspicious_a', 'suspicious_b'], time_window=1)

    hits = short_seq.alert(events)

    assert hits is None


