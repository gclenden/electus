from __future__ import unicode_literals
import pytest
import os
import pkgutil
from distutils import dir_util

from electus import LibraryDefinition
from electus.config import Library, Job
from electus.combinations import WeightedFilter, Sequence, Join

indicators = [{'field': 'process', 'value': 'powershell.exe'},
              {'field': 'parent_process', 'value': 'winword.exe'}]
definition = {'datetime_field': 'datetime', 'weight': 10.0,
              'indicators': indicators}

lib_def = LibraryDefinition(name='word_launching_powershell', definition=definition)

@pytest.fixture
def datadir(tmpdir, request):
    """Copies test data to a temporary directory"""
    filename = request.module.__file__
    test_dir, _ = os.path.splitext(filename)
    if os.path.isdir(test_dir):
        dir_util.copy_tree(test_dir, str(tmpdir))

    return tmpdir

# Testing library object
def test_load_library_instance():
    lib = Library({'word_launching_powershell': definition})
    assert lib.library[0].name == 'word_launching_powershell'

def test_load_library_from_file(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))

    assert isinstance(valid_lib.library, list)

def test_file_not_found(datadir):
    with pytest.raises(IOError):
        Library(str(datadir.join('does_not_exist.json')))

# Testing job object
def test_invalid_job_name(datadir):

    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_no_thresh = {"winword_launching_powershell": {"type": "filter", "features": ["word_launching_powershell"]}} 
    # Name needs to be a non empty string
    with pytest.raises(ValueError):
        Job(12345, filter_no_thresh["winword_launching_powershell"], valid_lib.library)

def test_invalid_job_type(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_no_thresh = {"winword_launching_powershell": {"type": "NOT_VALID", "features": ["word_launching_powershell"]}} 

    with pytest.raises(ValueError):
        Job("test", filter_no_thresh["winword_launching_powershell"], valid_lib.library)

def test_no_job_type(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_no_thresh = {"winword_launching_powershell": {"features": ["word_launching_powershell"]}} 
    
    with pytest.raises(ValueError):
        Job("test", filter_no_thresh["winword_launching_powershell"], valid_lib.library)

def test_feature_not_found(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_no_thresh = {"winword_launching_powershell": {"type": "filter", "features": ["DOES_NOT_EXIST"]}} 
    
    with pytest.raises(ValueError):
        Job("test", filter_no_thresh["winword_launching_powershell"], valid_lib.library)

def test_feature_not_a_list(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_no_thresh = {"winword_launching_powershell": {"type": "filter", "features": "NOT_A_LIST"}} 
    
    with pytest.raises(ValueError):
        Job("test", filter_no_thresh["winword_launching_powershell"], valid_lib.library)

def test_no_feature_key(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_no_thresh = {"winword_launching_powershell": {"type": "filter"}} 

    with pytest.raises(ValueError):
        Job("test", filter_no_thresh["winword_launching_powershell"], valid_lib.library)

def test_filter_default_threshold(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_no_thresh = {"winword_launching_powershell": {"type": "filter", "features": ["word_launching_powershell"]}} 
    test_job = Job("winword_launching_powershell", filter_no_thresh["winword_launching_powershell"], valid_lib.library)

    assert test_job.name == "winword_launching_powershell"
    assert test_job.job_type == "filter"
    assert isinstance(test_job.combination_engine, WeightedFilter)

def test_filter_with_threshold(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    filter_conf = {"winword_launching_powershell": {"type": "filter", "threshold": 10, "features": ["word_launching_powershell"]}}
    test_job = Job("winword_launching_powershell", filter_conf["winword_launching_powershell"], valid_lib.library)

    assert test_job.name == "winword_launching_powershell"
    assert test_job.job_type == "filter"
    assert isinstance(test_job.combination_engine, WeightedFilter)

def test_join(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    join_conf = {"temp.exe and bad.exe": {"type": "join", "features":["temp.exe", "bad.exe"], "join_expression": "temp.exe and bad.exe"}}
    test_job = Job("temp.exe and bad.exe", join_conf["temp.exe and bad.exe"], valid_lib.library)

    assert test_job.name == "temp.exe and bad.exe"
    assert test_job.job_type == "join"
    assert isinstance(test_job.combination_engine, Join)

def test_join_no_expr(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    join_conf = {"temp.exe and bad.exe": {"type": "join", "features":["temp.exe", "bad.exe"]}}
    
    with pytest.raises(ValueError):
        Job("temp.exe and bad.exe", join_conf["temp.exe and bad.exe"], valid_lib.library)

def test_sequence(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    seq_conf = {"temp.exe then bad.exe": {"type": "sequence", "features":["temp.exe", "bad.exe"], "sequence": ["temp.exe", "bad.exe"], "time_window": 5}}
    test_job = Job("temp.exe then bad.exe", seq_conf["temp.exe then bad.exe"], valid_lib.library)

    assert test_job.name == "temp.exe then bad.exe"
    assert test_job.job_type == "sequence"
    assert isinstance(test_job.combination_engine, Sequence)

def test_sequence_no_time_window(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    seq_conf = {"temp.exe then bad.exe": {"type": "sequence", "features":["temp.exe", "bad.exe"], "sequence": ["temp.exe", "bad.exe"]}}
    
    with pytest.raises(ValueError):
        Job("temp.exe then bad.exe", seq_conf["temp.exe then bad.exe"], valid_lib.library)

def test_sequence_no_key(datadir):
    valid_lib = Library(str(datadir.join('valid_library.json')))
    seq_conf = {"temp.exe then bad.exe": {"type": "sequence", "features":["temp.exe", "bad.exe"], "time_window": 5}}
    
    with pytest.raises(ValueError):
        Job("temp.exe then bad.exe", seq_conf["temp.exe then bad.exe"], valid_lib.library)
