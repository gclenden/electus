.. electus documentation master file, created by
   sphinx-quickstart on Tue Feb 27 19:00:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to electus's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :hidden:

   api

.. include:: tutorial.inc


