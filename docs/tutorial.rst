.. currentmodule:: electus

Getting started
~~~~~~~~~~~~~~~~

Electus is a package for generating alerts based on combinations of indicators. It allows the user to create a library of flexible behavioural signatures 
that can be reused in different contexts. To install electus run::

    pip install electus

Once the signatures have been defined they can be combined in different ways (called jobs). For example, one 
type of job is a sequence which looks for events in a specific order within a given time window.

A very basic example is ::

    from electus import Electus

    # Create some sample data
    events = [{
               "process": "winword.exe", 
               "parent_process": "explorer.exe",
               "datetime": "2017-10-16T17:15:47.030114"
               },
              {"process": "powershell.exe",
               "parent_process": "winword.exe",
               "datetime": "2017-10-16T17:15:48.030123"
              }]

    e = Electus(library_conf='library.json', job_conf='jobs.json')

    for event in events:
        alerts = e.evaluate_event(event)
        if alerts:
            print(alerts)

Defining a library of indicators
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Indicators are the fundamental unit within electus. Given an event with certain fields, they are used to match specified key-value pairs.

Indicators are defined in a json file (typically called library.json). For example a very basic library.json (with one indicator) looks like

::

    {
      "word_launching_powershell": 
      {
        "datetime_field": "datetime",
        "indicators": [{"field": "process", "value": "powershell.exe"},
                       {"field": "parent_process", "value": "winword.exe"}]
      }
    }

This will match events where the process field is "powershell.exe" and the "parent_process" field is "winword.exe". Each entry in the library requires
a unique name ("word_launching_powershell" in this case) and the following fields:

:datetime_field: This is the key in the event used to compare timestamps where necessary.
:indicators: This is a list of dictionaries with the following fields:

    :field: The key to look for in the event object.
    :value: The value to match on in the event object.
    :match: (Optional) Whether or not to match that object. Defaults to true.
    :regex: (Optional) If the value is to be used is a regex (otherwise a straight comparison will be used). Defaults to false.
    :case_sensitive: (Optional) Whether or not to do case sensitive comparisons. Defaults to true.
:weight: (Optional) A score to assign if all of the indicators match. Used in the filter job.

.. note::

    The name can only contain alphanumeric characters (plus underscores) and cannot start with a number. This is due to the way the boolean logic is parsed
    for join jobs.


A larger example library.json is shown below:

::

    {
      "word_launching_powershell": {"datetime_field": "datetime",
                                    "weight": 10.0,
                                    "indicators": [{"field": "process", "value": "powershell.exe"},
                                                   {"field": "parent_process", "value": "winword.exe"}]

                                   },
      "temp.exe": {"datetime_field": "datetime",
                                    "indicators": [{"field": "process", "value": ".*temp.exe", "is_regex": true, "case_sensitive":false}]
                                   },
      "bad.exe": {"datetime_field": "datetime",
                                    "indicators": [{"field": "process", "value": "bad.exe"}]
                                   }
    }

Combining indicators
~~~~~~~~~~~~~~~~~~~~

Jobs are used to combine sets of indicators to alert on events of
interest. The job definitions are typically stored in jobs.json. The
different job types are:

:Join: This can be used to combine indicators with boolean logic expressions. For example, the expression "A and (B or C)" will alert if
       *A and B* or *A and C* are seen. 
:Filter: Each definition in the library file can be weighted via the
   "weight" key. The filter job sums up all of the weights and alerts if
   it exceeds a specified threshold.
:Sequence: This alerts if an orded sequence of indicators occurs within a
           time window. For example, only alert if you see A then B then C
           within 10 seconds.

Join example
------------

Below is a basic example of using a join job

::

    "join_job_example": {"type": "join", "features":["temp.exe", "bad.exe"], "join_expression": "temp.exe and bad.exe"}

The following fields are required:

:type: This is the type of job (in this case join)
:features: This is a list of feature names specified in library.json
:join_expression: This is the logical statement for how to combine the indicators. AND, OR, &&, \|\| and parentheses are supported.

Filter example
--------------

The filter job looks similar to the join job:

::

    "winword_launching_powershell": {"type": "filter", "threshold": 10, "features": ["word_launching_powershell"]},

:type: The type of job (in this case filter)
:threshold: This is the minimum score for alerting. In this case the sum of scores must be >=10
:features: This is a list of feature names specified in library.json.

Sequence example
----------------

In this example we are looking for temp.exe then bad.exe within a span
of 5 seconds.

::

    "temp.exe then bad.exe": {"type": "sequence", "features":["temp.exe", "bad.exe"], "sequence": ["temp.exe", "bad.exe"], "time_window": 5}

:sequence: This is the list of indicators names from the library in the order they should be seen
:time_window: This is the total time window (in seconds) to look for the sequence in.

