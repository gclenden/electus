.. _api-ref:

Electus API reference
=====================

.. automodule:: electus
   :members:

Alert
--------------------------

.. automodule:: electus.alert

.. autoclass:: Alert
  :members:
  :member-order: bysource

Combinations
--------------------------

.. automodule:: electus.combinations

.. autoclass:: Join
  :members:
  :member-order: bysource

.. autoclass:: Sequence
  :members:
  :member-order: bysource

.. autoclass:: WeightedFilter
  :members:
  :member-order: bysource

Config
------

.. automodule:: electus.config

.. autoclass:: Job
   :members:
   :member-order: bysource

.. autoclass:: Library
   :members:
   :member-order: bysource

Electus
--------------------------

.. automodule:: electus.main

.. autoclass:: Electus
  :members:
  :member-order: bysource

Library
--------

.. automodule:: electus.library

.. autoclass:: Indicator
   :members:
   :member-order: bysource

.. autoclass:: LibraryDefinition
   :members:
   :member-order: bysource
